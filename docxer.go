package docxer

/* import necessary libs */
import (
//	"encoding/xml"
	"time"

	"github.com/google/uuid"
)

/* constants */
const (
	CXmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
)

/* some preliminary informations */

/*
 * DOCX - format which microsoft introduced in 2007. Although world wanted Open format, microsoft
 * again went by their own way and made up something which i would call total mess.
 * From start to end, that composite is total mess with relations cross relations, all in xml.
 * all those structures,xmls, references are zip packed into 1 file with docx extension
 * basic structure of simple document itself is mess even.
 * some introduction can be found here: https://www.toptal.com/xml/an-informal-introduction-to-docx
 *
 * so basicaly clean document with 1 word :
 *
 * -[Content_Types].xml  	--> which ill head came with main content reference with [] in file name ?
 * - word					--> dir which should contain ...
 *		|-->_rels			--> subdir ? WHY ?
 *		|		|-->document.xml.rels --> references for styles, sources, etc...
 *		|		|-->.rels	--> another references xml, why . on start ? why no ext ?
 *		|-->document.xml	--> main content file with text, all wrapped in xml with tags, references and so
 *		|-->styles.xml		--> xmls with styles specs, all with some references to god knows where
 *
 */

/* type Document -> basic type which contains basic metadata and basic informations */
type Document struct {
	Uuid    uuid.UUID   `xml:"document_id,omitempty"`
	Name    string      `xml:"document_name"`
	Author  string      `xml:"author"`
	Created time.Time   `xml:"created"`
	Styles  interface{} `xml:"styles"`     /* for now empty iface, will be changed */
	Refs    interface{} `xml:"references"` /* for now empty iface, will be changed */
	Packed  bool        /* is document already packed to zip ? */
	Raw     []byte      /* raw data when document is packed */
}

// NewDocument - function for create new document object
func NewDocument(docname string) (*Document, error) {
	// first create doc struct
	docx := &Document{}
	// now create basic metadata - uuid
	auid, err := uuid.NewUUID()
	if err != nil {
		return nil, err
	}
	// get actual time for created timestamp
	atime := time.Now()
	// fill in basic metadata
	docx.Name = docname
	docx.Uuid = auid
	docx.Created = atime
	return docx, nil
}
